from collections import defaultdict

ip_sess_filename = 'ip_address_session_duration.txt'
ip_address_session_time = dict()
with open(ip_sess_filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        info = line.split(',')
        ip_address = info [0]
        sess_time = info[1:]
        if len(sess_time)==0:
            continue
        sess_time = [float(x.strip()) for x in sess_time]
        average_time = sum(sess_time)*1.0/len(sess_time)
        ip_address_session_time[ip_address] = average_time

ip_location_filename = 'ip_address_city_name.txt'

location_ip_address = defaultdict(list)
with open(ip_location_filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        info = line.split(',')
        ip_address = info[0]
        location = info[1].strip()
        if len(location) == 0:
            continue
        location_ip_address[location].append(ip_address)

location_session_time = defaultdict(list)
for location, ip_address_list in location_ip_address.items():
    for ip_address in ip_address_list:
        average_time = str(ip_address_session_time.get(ip_address, 0))
        location_session_time[location.strip()].append(average_time)

location_sess_time_filename = 'location_sess_time.txt'
with open(location_sess_time_filename, 'w+') as f:
    for location, average_time in location_session_time.items():
        f.write(','.join([location] + average_time) + '\n')

location_avg_sess_time_filename = 'location_avg_sess_time.txt'
with open(location_avg_sess_time_filename, 'w+') as f:
    for location, average_time in location_session_time.items():
        average_time = [float(x) for x in average_time]
        avg_time = str(sum(average_time)/len(average_time))
        f.write(','.join([location] + [avg_time]) + '\n')
