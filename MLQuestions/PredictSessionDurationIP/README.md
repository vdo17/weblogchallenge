# README #

This is a note for predicting session duration given an IP

From an IP, we can learn the location. I assume that people in a same location will have browse PayTM in a similar duration. For example, people in the city might be more engaged and enthusiastic and hence spent more time. 

### Data_Processing step ###
I use the tool from here: https://freegeoip.net/?q=76.10.168.131 to convert IP Address to City. The implementation is in get_location_ipaddress_geoip.py
Then I aggreate data into city: session duration using ip_address_session_duration generated from the previous question. 
Output is location_sess_time.txt and location_avg_sess_time.txt. File: process_city_session_time.py. 

### Training ###
I think if an IP coming from a city that we have in location_avg_sess_time.txt, we would output assign the predicted value to be the aveage session time of that city.
Otherwise, I would use take the average of the k Nearest Neighbor (we need to experiment with k) with the nearest neighbors to be surrounding cities. Unfortunately I do not have the time to implement a function to calcuate distance between cities to investigate how well this approach would work
###Runing Guide ###
python get_location_ipaddress_geoip.py
python process_city_session_time.py
