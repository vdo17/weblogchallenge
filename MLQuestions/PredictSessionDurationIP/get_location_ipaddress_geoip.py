from time import sleep
import requests

FREEGEOPIP_URL = 'http://freegeoip.net/json'
#folder_name = '/h/143/vando/WeblogChallenge/data/pyipinfodb-master/'
folder_name ='/Users/davidmcintosh/Documents/HaiVan/weblogchallenge/data/MLQuestions/'
filename = folder_name + '2015_07_22_mktplace_shop_web_log_sample.log'

def get_ip_address(line):
    line_list = line.split()
    return line_list[2].split(':')[0]

def get_geolocation_for_ip(ip):
    url = '{}/{}'.format(FREEGEOPIP_URL, ip)

    response = requests.get(url)
    response.raise_for_status()

    return response.json()

ip_address_city_name_dict = {}

NUMBER = 3

print (NUMBER)
input_filename = folder_name + 'ip_address_set_{i}.txt'.format(i=NUMBER)
#input_filename = folder_name + 'test.txt' #'ip_address_set_{i}.txt'.format(i=NUMBER)
output_filename = folder_name + 'ip_address_city_name_{i}.txt'.format(i=NUMBER)

with open(input_filename, 'r') as f:
    ip_address_set = [x.strip() for x in f.readlines()]

for ip_address in ip_address_set:
    while True:
        try:
            location_info = get_geolocation_for_ip(ip_address)
            #print ('success', ip_address)
            break
        except Exception as e:
            print (e)
    city_name = location_info.get('city', None)
    if city_name is not None:
        ip_address_city_name_dict[ip_address] = city_name

with open(output_filename, 'w+') as f:
    for key, value in ip_address_city_name_dict.items():
        try:
            f.write(','.join([key, value]) + '\n')
        except:
            #import pdb; pdb.set_trace()
            print (key, value)
            continue
            #import pdb; pdb.set_trace()

