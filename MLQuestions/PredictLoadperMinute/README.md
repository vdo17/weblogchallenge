# README #

This is a note for predicting load per minutes

### Data_Processing step ###
- Compute load per minute or per second
- I decide to only have 1 training instance of [0]*N. This is because if we use these 0 features, the classifiers would tend to predict everything to be 0. 
I think that we could set a hard set rule: if features are all zeros, the predicted load will be zero

### Training ###
- I observe that loads are high for a few minutes (1.5 minutes) then a long time of zero activities.
- I assume that predicted load per minute/second depends on load in previous N minutes/seconds. N is a parameter I try to optimize.
- Features for training are loads per minute/second of previous minutes
- I choose to use Linear Regression(LR) model because the data are fairly simple to model. 
- The score (R square) chosen to be the evaluation metrics
- When I predict load/minute using load/minute, I get score in a range of 0.3 to the highest of 0.7 for N features ranging from 1 to 7
- When I predict load/second using load/second data, I get score of 0.9 for 60 features. I think this model gives a good prediction. To get load/second, I could taken load/second*60

###Runing Guide ###
python data_processing_ML_challenge.py
python predict_loads_per_minute.py
