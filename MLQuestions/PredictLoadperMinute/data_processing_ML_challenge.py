'''
Process data for Predicting loads per minutes
General ideas:
- Start time is the first access time in the log
- Take the difference (in minutes or seconds) between an access time and start time
- Create a list with index is the time difference above and value to be the number of loads/minutes or seconds
- Write to training and test file
'''

from datetime import datetime
from datetime import timedelta
from collections import defaultdict
filename = '../../data/sorted_2015_07_22_mktplace_shop_web_log_sample.log'
#filename = '2015_07_22_mktplace_shop_web_log_sample.log'
FMT = "%Y-%m-%dT%H:%M:%S.%fZ"

def get_access_time(line):
    line_list = line.split()
    return datetime.strptime(line_list[0], FMT)

def save_file(count_list, filename):
   count_list = [str(x) for x in count_list]
   with open(filename,'w+') as f:
       f.write(','.join(count_list))
   print ('done writting', filename)

def save_data_label_file(data, data_filename):
   with open(data_filename, 'w+') as f:
       f.write('\n'.join(data))
   print ('done writing', data_filename)

def extract_data(count_list, n_features):
   data = set() # first n item in each list is n features,
   #and the last one is predicted value
   for i in range(len(count_list)):
       if (i + n_features) > len(count_list)-1:
           break
       n_features_label = count_list[i : i+n_features+1]
       n_features_label = [str(x) for x in n_features_label]
       temp_string = ','.join(n_features_label)
       data.update([temp_string])
   return [x for x in data]

with open(filename, 'r') as f:
   lines = f.readlines()
   START_TIME = get_access_time(lines[0])
   END_TIME = get_access_time(lines[-1])
   time_delta = END_TIME - START_TIME
   num_minutes = int(time_delta.total_seconds()/60.0)+1
   num_seconds = int(time_delta.total_seconds())+1
   time_delta = END_TIME - START_TIME
   time_diff_count = [0]*num_minutes
   time_diff_count_seconds = [0]*num_seconds


   index_set= set()
   index_set_seconds= set()
   for line in lines[1:]:
       time_diff = get_access_time(line) - START_TIME
       index = int(time_diff.total_seconds()/60.0)
       index_seconds = int(time_diff.total_seconds())
       #print index
       index_set.update([index])
       index_set_seconds.update([index_seconds])
       if index < 0:
           import pdb; pdb.set_trace()
       try:
          time_diff_count[index] += 1
          time_diff_count_seconds[index_seconds] += 1
       except:
          import pdb; pdb.set_trace()
   folder_name = '' #/h/143/vando/WeblogChallenge/data/'
   save_file(time_diff_count, folder_name + 'count_per_minutes.txt')
   save_file(time_diff_count_seconds, folder_name + 'count_per_seconds.txt')
   NUM_FEATURES = 60
   data = extract_data(time_diff_count, n_features=NUM_FEATURES)
   print ('data per minutes',len(data))
   data_train = data[:int(0.8*len(data))]
   data_test = data[int(0.8*len(data)):]

   save_data_label_file(data, folder_name + 'data_per_minutes_n{k}.txt'.format(k=NUM_FEATURES))
   save_data_label_file(data_train, folder_name + 'train_data_per_minutes_n{k}.txt'.format(k=NUM_FEATURES))
   save_data_label_file(data_test, folder_name + 'test_data_per_minutes_n{k}.txt'.format(k=NUM_FEATURES))

   data_seconds = extract_data(time_diff_count_seconds, n_features=NUM_FEATURES)
   print ('data per seconds',len(data_seconds))
   data_seconds_train = data_seconds[:int(0.8*len(data))]
   data_seconds_test = data_seconds[int(0.8*len(data)):]

   save_data_label_file(data_seconds, folder_name + 'data_per_seconds_n3.txt')
   save_data_label_file(data_seconds_train, folder_name + 'train_data_per_seconds_n{k}.txt'.format(k=NUM_FEATURES))
   save_data_label_file(data_seconds_test, folder_name + 'test_data_per_seconds_n{k}.txt'.format(k=NUM_FEATURES))
