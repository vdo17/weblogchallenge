import sklearn
import numpy as np

folder_name = ''
NUM_FEATURES = 60
#train_filename = folder_name + 'train_data_per_minutes_n{k}.txt'.format(k=NUM_FEATURES)
#test_filename = folder_name + 'test_data_per_minutes_n{k}.txt'.format(k=NUM_FEATURES)

train_filename = folder_name + 'train_data_per_seconds_n{k}.txt'.format(k=NUM_FEATURES)
test_filename = folder_name + 'test_data_per_seconds_n{k}.txt'.format(k=NUM_FEATURES)
from sklearn.model_selection import cross_val_predict
from sklearn import linear_model


def read_data(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        X = []
        y = []
        for line in lines:
            temp_data = [float(x) for x in line.split(',')]
            if len(temp_data)!= NUM_FEATURES + 1:
               import pdb;pdb.set_trace()
            X.append(temp_data[:-1])
            y.append(temp_data[-1])
    #X = np.array(X)
    return np.array(X), np.array(y)

def main():
   X_train, y_train = read_data(train_filename)
   import pdb; pdb.set_trace()
   lr = linear_model.LinearRegression()
   trained_model = lr.fit(X_train, y_train)

   X_test, y_test = read_data(test_filename)
   print (trained_model.score(X_test, y_test))
   empty_test = np.array([0]*NUM_FEATURES).reshape(-1, NUM_FEATURES)
   predicted_y = trained_model.predict(empty_test)
   print predicted_y

main()
