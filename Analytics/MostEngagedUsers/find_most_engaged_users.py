'''
Goal: Find the most engaged users
Output:
- Print out the IP address and number of minutes the longest sessions.

Data structure explaination:
- ip_address_access_time_dict: {ip_address: [access_time1, access_time2, ... , access_timeN]}
- ip_address_session_dict: {ip_address: [session1, session2, ...,  sessionN_time]} with each session is a list of access times in a 15-minute session
'''

from collections import defaultdict
from datetime import timedelta
from datetime import datetime
from utils import preprocess_data, break_to_sessions

ip_address_access_time_dict = defaultdict(list)
ip_address_session_dict = defaultdict(list)

max_session_time = 0.0
max_ip = defaultdict(list)

def most_engage_users_sessions_time(key, sessions):
    assert len(sessions)!=0, 'sessions is empty'
    global max_session_time, max_ip
    for session in sessions:
        session_time = session[-1]-session[0]
        session_time = session_time.total_seconds()
        if session_time< 0:
            import pdb; pdb.set_trace()
        if session_time==0:
            continue
        if max_session_time <= session_time:
            max_session_time = session_time
            max_ip[max_session_time].append(key)

filename = '../../data/sorted_2015_07_22_mktplace_shop_web_log_sample.log'

with open(filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        access_time, ip_address, _ = preprocess_data(line)
        ip_address_access_time_dict[ip_address].append(access_time)
    print 'done preprocess data'
    
    for key, access_time_list in ip_address_access_time_dict.items():
        sessions = break_to_sessions(access_time_list, time_limit=15)
        ip_address_session_dict[key] = sessions
        if len(sessions)==0:
            print (key, 'has no session', sessions)
        most_engage_users_sessions_time(key, sessions)
    max_time = max(max_ip.keys())
    print ('Most engaged users', max_ip[max_time],
           'with max session time', max_time/60,
           'minutes'
          )
