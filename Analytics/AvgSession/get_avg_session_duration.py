'''
Goal:Get average session time.
Output:
- Print out the average session time for all hits
- Write a file with ip_address: [session1_time, session2_time, ..., sessionN_time])
NOTES:
- Skip sessions with only 1 click i.e. session duration = 0
Data structure explanation:
- ip_address_access_time_dict: {ip_address: [access_time1, access_time2, ... , access_timeN]}
- ip_address_session_dict: {ip_address: [session1, session2, ...,  sessionN_time]} with each session is a list of access times in a 15-minute session
- ip_address_session_duration_dict: {ip_address:[session1_duration, session2_duration, ..., sessionN_duration]} 
'''

from collections import defaultdict
from datetime import timedelta
from datetime import datetime
from utils import preprocess_data, break_to_sessions

ip_address_access_time_dict = defaultdict(list)
ip_address_session_dict = defaultdict(list)
ip_address_session_duration_dict = defaultdict(list)

num_sess = 0
ip_session_time = 0

def get_ip_address_session_time_dict(key, sessions, w):
    global ip_session_time, num_sess
    if len(sessions)==0:
        print ('sessions with', key, 'is empty')
    for session in sessions:
        session_time = session[-1]-session[0]
        session_time = session_time.total_seconds()
        if session_time < 0:
            import pdb; pdb.set_trace()
        if session_time == 0:
            continue
        ip_address_session_duration_dict[key].append(str(session_time))
        ip_session_time += session_time
        num_sess += 1
    w.write(','.join([key] + ip_address_session_duration_dict[key]) + '\n')

filename = '../../data/sorted_2015_07_22_mktplace_shop_web_log_sample.log'
out_filename = 'ip_address_session_duration.txt'
w = open(out_filename, 'w+')

with open(filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        access_time, ip_address, _ = preprocess_data(line)
        ip_address_access_time_dict[ip_address].append(access_time)
    print 'done preprocess data'
    
    for key, access_time_list in ip_address_access_time_dict.items():
        sessions = break_to_sessions(access_time_list, time_limit=15)
        ip_address_session_dict[key] = sessions
        if len(sessions)==0:
            print (key, 'has no session', sessions)
        get_ip_address_session_time_dict(key, sessions, w)
    print ('Average session time ', ip_session_time/num_sess)

