from collections import defaultdict
from datetime import timedelta
from datetime import datetime
from utils import preprocess_data, break_to_sessions

'''
Sessionize the web log by IP. Sessionize = aggregrate all page hits by visitor/IP during a fixed time window.
'''
ip_address_session_dict = defaultdict(list)
ip_address_session_count_dict = defaultdict(dict)
ip_address_access_time_dict = defaultdict(list)
ip_sessions_time = timedelta(seconds=0)

def count_hits(key, sessions):
   ip_address_session_count_dict.update({key: [len(session) for session in sessions]})

def write_dict_to_file(output_filename, dict_to_write):
    with open(output_filename, 'w+') as f:
        for ip_address, sessions in dict_to_write.items():
            sessions = [str(x) for x in sessions]
            if sessions[0] is list:
                sessions = ['-'.join(x) for x in sesssions]
            f.write(','.join([ip_address] + sessions) +'\n')

filename = '../../data/sorted_2015_07_22_mktplace_shop_web_log_sample.log'
with open(filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        access_time, ip_address, _ = preprocess_data(line)
        ip_address_access_time_dict[ip_address].append(access_time)
    print 'done preprocess data'
    for key, access_time_list in ip_address_access_time_dict.items():
        sessions = break_to_sessions(access_time_list, time_limit=15)
        ip_address_session_dict[key] = sessions
        if len(sessions)==0:
            print (key, 'has no session', sessions)
            import pdb; pdb.set_trace()
        count_hits(key, sessions)
output_filename = 'ip_address_session_dict.txt'
write_dict_to_file(output_filename, ip_address_session_dict)
output_filename = 'ip_address_session_count_dict.txt'
write_dict_to_file(output_filename, ip_address_session_count_dict)
