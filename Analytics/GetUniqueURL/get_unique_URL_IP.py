from collections import defaultdict
from datetime import timedelta
from datetime import datetime
from utils import preprocess_data, break_to_sessions
'''
Goal: get unique URL per session

Outtput:
- ip_address_count_unique_URL.txt: ip address: count of unique URL per session
- ip_address_unique_URL.txt: ip address: [URLs of sess1], [URLs of sess2]
'''

ip_address_access_time_dict = defaultdict(list)
ip_address_URL_dict = defaultdict(list)

def unique_URL_visit_per_session(key, sessions, write_count, write_URL):
    URLs = ip_address_URL_dict.get(key, [])
    assert len(URLs)!=0
    start_index = 0
    end_index = 0
    unique_URL_sessions = []
    for session in sessions:
        end_index += len(session)
        unique_URL_per_session = list(set(URLs[start_index:end_index]))
        assert len(unique_URL_per_session)>0
        start_index += len(session)
        unique_URL_sessions.append(unique_URL_per_session)
    unique_URL_count = [str(len(x)) for x in unique_URL_sessions]
    write_count.write(','.join([key] + unique_URL_count) + '\n')
    unique_URL_per_session = [';'.join(x) for x in unique_URL_sessions]
    write_URL.write(','.join([key] + unique_URL_per_session)+'\n')

count_output_filename = 'ip_address_count_unique_URL.txt'
URL_output_filename = 'ip_address_unique_URL.txt'
filename = '../../data/sorted_2015_07_22_mktplace_shop_web_log_sample.log'
write_count = open(count_output_filename, 'w+')
write_URL = open(URL_output_filename, 'w+')

with open(filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        access_time, ip_address, URL = preprocess_data(line)
        ip_address_access_time_dict[ip_address].append(access_time)
        ip_address_URL_dict[ip_address].append(URL)
    print 'done preprocess data'
    
    for key, access_time_list in ip_address_access_time_dict.items():
        sessions = break_to_sessions(access_time_list, time_limit=15)
        if len(sessions)==0:
            print (key, 'has no session', sessions)
        unique_URL_visit_per_session(key, sessions, write_count, write_URL)
