from collections import defaultdict
from datetime import timedelta
from datetime import datetime
'''
Utils functions
'''
def break_to_sessions(access_time_list, time_limit=15):
    assert len(access_time_list)!=0, 'access_time_list has no item'
    result=[]
    start_index = 0
    end_index =0
    #access_time_list.sort()
    for i in range(len(access_time_list)-1):
        if access_time_list[i] < access_time_list[i+1] - timedelta(minutes=time_limit):
            # this is a new session
            #import pdb; pdb.set_trace()
            end_index = i+1
            result.append(access_time_list[start_index:end_index])
            start_index = i+1
    result.append(access_time_list[start_index:])
    return result

def preprocess_data(line):
    line_list = line.split()
    fmt =  "%Y-%m-%dT%H:%M:%S.%fZ"
    access_time = datetime.strptime(line_list[0], fmt)
    ip_address = line_list[2].split(':')[0]
    URL = line_list[12]
    #ip_address_URL_dict[ip_address].append(URL)
    #ip_address_access_time_dict[ip_address].append(access_time)
    return access_time, ip_address, URL
