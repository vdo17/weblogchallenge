'''
Goal:Get average session time.
'''
from pyspark.sql.functions import coalesce, col, datediff, lag, lit, sum as sum_, when, isnull, unix_timestamp

from pyspark.sql.window import Window
from pyspark.sql import Row
from datetime import datetime

def preprocess_data(line):
    line_list = line.split()
    access_time = line_list[0]#datetime.strptime(line_list[0], fmt)
    ip_address = line_list[2].split(':')[0]
    URL = line_list[12]
    return Row (ip_address=ip_address, access_time=access_time, URL=URL)

def time_diff(x, y):
    fmt =  "%Y-%m-%dT%H:%M:%S.%fZ"
    time_delta = (datetime.strptime(x, fmt) - datetime.strptime(y, fmt)).total_seconds()
    if time_delta:
        return time_delta
    else:
        return 0


filename = '2015_07_22_mktplace_shop_web_log_sample.log'
text_file = sc.textFile(filename)

#read data
base_df = text_file.map(preprocess_data).toDF()
w = Window.partitionBy("ip_address").orderBy("access_time")

base_df = base_df.withColumn("prev_access_time", lag(base_df.access_time).over(w))

sqlContext.registerFunction("time_diff", time_diff )

base_df.createOrReplaceTempView("Test_table")

#count number of session per ip_address
spark.sql("SELECT ip_address, count(ip_address) as count FROM (SELECT ip_address, time_diff(access_time, prev_access_time) as time_delta FROM Test_table)ue WHERE ue.time_delta>60*15 group by ip_address").show()

#average session time
spark.sql("SELECT s.ip_address as ip_address, s.sum/c.count FROM  (SELECT ip_address, count(ip_address) as count FROM (SELECT ip_address, time_diff(access_time, prev_access_time) as time_delta FROM Test_table) group by ip_address)c JOIN (SELECT ip_address, sum(time_delta) as sum  FROM (SELECT ip_address, time_diff(access_time, prev_access_time) as time_delta FROM Test_table) group by ip_address)s ON c.ip_address=s.ip_address");

##unique URL per session
spark.sql("SELECT ip_address, session_grouping, count( DISTINC URL) FROM (SELECT *, sum(case when time_delta > 15*60 then 1 else 0 end) over (partition by ip_address order by access_time) as session_grouping FROM (SELECT ip_address, URL, access_time, time_diff(access_time, prev_access_time) as time_delta FROM Test_table)) group by ip_address, session_grouping").show()


#IP with the longest session time
spark.sql("SELECT ip_address FROM (SELECT ip_address, session_grouping, sum(time_delta) as session_time FROM (SELECT *, sum(case when time_delta > 15*60 then 1 else 0 end) over (partition by ip_address order by access_time) as session_grouping FROM (SELECT ip_address, URL, access_time, time_diff(access_time, prev_access_time) as time_delta FROM Test_table)) group by ip_address, session_grouping) WHERE max(session_time)==session_time").show()

ip_s = spark.sql("SELECT ip_address, sum(time_delta) as session_time FROM (SELECT *, sum(case when time_delta > 15*60 then 1 else 0 end) over (partition by ip_address order by access_time) as session_grouping FROM (SELECT ip_address, URL, access_time, time_diff(access_time, prev_access_time) as time_delta FROM Test_table)) group by ip_address, session_grouping")

max_s = spark.sql("SELECT max(session_time) as max_session_time FROM (SELECT ip_address, session_grouping, sum(time_delta) as session_time FROM (SELECT *, sum(case when time_delta > 15*60 then 1 else 0 end) over (partition by ip_address order by access_time) as session_grouping FROM (SELECT ip_address, URL, access_time, time_diff(access_time, prev_access_time) as time_delta FROM Test_table)) group by ip_address, session_grouping)")

ip_s.createOrReplaceTempView("ip_s")
max_s.createOrReplaceTempView("max_s")

spark.sql("SELECT ip_address FROM ip_s FULL JOIN max_s WHERE ip_s.session_time = max_s.max_session_time").show()
